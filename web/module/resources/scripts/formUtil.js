/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
function onlyNumbers(evt) {
    var e = event || evt; // for trans-browser compatibility
    var charCode = e.which || e.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function convertDateFormat(datePattern) {
	var datePatternStart = datePattern.substr(0,1).toLowerCase();
	var datePatternSeparator = datePattern.substr(2,1).toLowerCase();

    if (datePatternStart == 'm') { /* M-D-Y */
		return 'm' + datePatternSeparator + 'd' + datePatternSeparator + 'Y';
    } else if (datePatternStart == 'y') { /* Y-M-D */
		return 'Y' + datePatternSeparator + 'm' + datePatternSeparator + 'd';
    } else { /* D-M-Y */
		return 'd' + datePatternSeparator + 'm' + datePatternSeparator + 'Y';
    }
}