<%--
Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.

Reproduction or distribution of this source code is prohibited.
--%>
<%
String[] lang = org.openmrs.api.context.Context.getLocale().toString().split("_");
pageContext.setAttribute("lang", lang[0]);
%>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/resources/css/ext-all.css"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/adapter/ext/ext-base.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/ext-all.js"/>
<openmrs:htmlInclude file="/moduleResources/jslibs/scripts/ext-3.2.1/src/locale/ext-lang-${lang}.js"/>
